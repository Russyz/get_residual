**Calculating laser ranging O-C residual from raw and pred data for TianQin Station**

保存文件命名方式
计算得到的4个通道的 ±10 \mu s 有效残差

> rec.c1, rec.c2, rec.c3, rec.c4

通过计算出的残差经过2.5 $\sigma$ 滤波

> rec.e1, rec.e2, rec.e3, rec.e4

column 1: time [seconds of day]

column 2: O-C residual [second]

column 3: radial velocity [mps]

column 4: time-of-flight [second]

如果某个通道没有数据，则不会生成对应通道的有效残差数据

生成laser ranging report, e.g.:

> lr_report_20200730

运行方法：

下载get_resudual解压后，打开get_resudual，安装所需要的包，在powershell命令栏输入:

`pip install -r requirements`

打开文件`satellite_match.py`,修改 fold_obs, fold_pro, fold_pre和fold_rep 文件夹的绝对路径，绝对路径命名方式参考台站保存文件的格式

对特定日期的的测量数据进行匹配和 2.5 \sigma 滤波 in terminal

`python satellite_match.py 20200730`

或者
对当天的的测量数据进行匹配和 2.5 \sigma 滤波 in terminal    

`python satellite_match.py`

